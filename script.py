"""
MAKE SURE YOU USE A VPN BEFORE RUNNING THIS CODE ON YOUR NETWORK

YOU DON'T WANT TO GIVE YOUR IP ADDRESS TO A HACKER

I AM NOT RESPONSIBLE FOR WHAT HAPPENS IF YOU RUN THIS CODE. BE SAFE!
"""

import random
import string
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.select import Select

import faker


def randomize_capitalization(string):
    n = int(random.random() * 100)
    if n in range(0,60):
        # 60% use default capitalization
        pass
    elif n in range(60,84):
        # 24% capitalize nothing
        return string.lower()
    elif n in range(84,99):
        # 15% capitalize only the first letter
        return string.lower().capitalize()
    else:
        # 1% have random capitalization
        return (''.join(random.choice((str.upper, str.lower))(c) for c in string))

    return string

# Fake Canadian goodness
fake = faker.Faker('en_CA')

# Fuck these guys
hacker_URL = "http://mth.jgr.mybluehost.me/"

chrome_options = webdriver.ChromeOptions()

window_positions = [0, 50, 100, 150, 200]

counter = 0

driver = webdriver.Chrome('chromedriver',options=chrome_options)
driver.set_window_size(640,480)
driver.set_window_position( random.choice(window_positions), random.choice(window_positions))

# Here we go
driver.get(hacker_URL)

WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.ID, "submitButton"))).click()

### PAYMENT PAGE ###
full_name_xpath = '//*[@id="fn"]'
address_xpath = '//*[@id="add"]'
appt_xpath = '//*[@id="add2"]'
city_xpath = '//*[@id="cty"]'
province_xpath = '//*[@id="prv"]'
postal_code_xpath = '//*[@id="post"]'
mobile_xpath = '//*[@id="mnum"]'
day_birth_xpath = '//*[@id="db1"]'
month_birth_xpath = '//*[@id="db2"]'
year_birth_xpath = '//*[@id="db3"]'

go_to_payment_xpath = '//*[@id="queryParcel:far_biz_common_fqp_continue"]'

# We re-create a fake name to potentially re-use later
FAKE = {
        "name": randomize_capitalization(fake.name()),
        "address" : str(random.randint(1,9999)) + " " + fake.street_name(),
        "phone" : fake.phone_number(),
        "day_birth" : random.choice([str(i).zfill(2) for i in range(1,32)]),
        "month_birth" : random.choice([str(i).zfill(2) for i in range(1,13)]),
        "year_birth" : random.choice([str(i) for i in range(1910,2003)]),
        "credit_card" : fake.credit_card_number(),
        "security_code" : fake.credit_card_security_code(),
        "maiden_name" : randomize_capitalization(fake.last_name()),
        "expiry_month" : random.choice([str(i).zfill(2) for i in range(1,13)]),
        "expiry_year" : random.choice([str(i).zfill(2) for i in range(2021,2041)]),
    }

driver.find_element_by_xpath(full_name_xpath).send_keys(FAKE['name'])
driver.find_element_by_xpath(address_xpath).send_keys(randomize_capitalization(FAKE['address']))

# Randomly input an appartment, since it's optional
if random.random() < 0.10:
    if random.random() < 0.70:
        FAKE['appt'] = random.randint(1,234)
        driver.find_element_by_xpath(appt_xpath).send_keys(FAKE['appt'])
    else:
        FAKE['appt'] = random.choice(string.ascii_letters)
        driver.find_element_by_xpath(appt_xpath).send_keys(randomize_capitalization(FAKE['appt']))

driver.find_element_by_xpath(city_xpath).send_keys(randomize_capitalization(fake.city()))

if random.random() < 0.5:
    FAKE['postal_code'] = fake.postalcode().lower()
else:
    FAKE['postal_code'] = fake.postalcode().upper()
driver.find_element_by_xpath(postal_code_xpath).send_keys(FAKE['postal_code'])

driver.find_element_by_xpath(mobile_xpath).send_keys(FAKE['phone'])

# Drop-down menus
Select(driver.find_element_by_xpath(day_birth_xpath)).select_by_visible_text(FAKE['day_birth'])
Select(driver.find_element_by_xpath(month_birth_xpath)).select_by_visible_text(FAKE['month_birth'])
Select(driver.find_element_by_xpath(year_birth_xpath)).select_by_visible_text(FAKE['year_birth'])

# Submit that garbage
driver.find_element_by_xpath(go_to_payment_xpath).click()


#### CARD INFO PAGE ####
name_on_card = '//*[@id="cfn"]'
card_number = '//*[@id="cnm"]'
expiry_month = '//*[@id="ce1"]'
expiry_year = '//*[@id="ce2"]'
security_code = '//*[@id="scv"]'
maiden_name = '//*[@id="mnm"]'
submit_button = '//*[@id="queryParcel_continue"]'

if random.random() < 0.50:
    FAKE['card_name'] = randomize_capitalization(fake.name())
else:
    # We re-use fake name from previously
    FAKE['card_name'] = FAKE['name']
driver.find_element_by_xpath(name_on_card).send_keys(FAKE['card_name'])
driver.find_element_by_xpath(card_number).send_keys(FAKE['credit_card'])
driver.find_element_by_xpath(security_code).send_keys(FAKE['security_code'])
driver.find_element_by_xpath(maiden_name).send_keys(FAKE['maiden_name'])
Select(driver.find_element_by_xpath(expiry_month)).select_by_visible_text(FAKE['expiry_month'])
Select(driver.find_element_by_xpath(expiry_year)).select_by_visible_text(FAKE['expiry_year'])

# SUBMIT, LOL
driver.find_element_by_xpath(submit_button).click()

# Sometimes, it finds my credit card numbers invalid...
try:
    invalid_number_element = driver.find_element_by_xpath("//*[@id=\"shp_block\"]/div[2]/div[1]/div[5]/div/div/small")
except:
    invalid_number_element = None
while invalid_number_element is not None:
    FAKE["credit_card"] = fake.credit_card_number()
    print("Trying a different credit card number,", FAKE["credit_card"])
    driver.find_element_by_xpath(card_number).clear()
    driver.find_element_by_xpath(card_number).send_keys(FAKE['credit_card'])
    driver.find_element_by_xpath(submit_button).click()
    try:
        invalid_number_element = driver.find_element_by_xpath("//*[@id=\"shp_block\"]/div[2]/div[1]/div[5]/div/div/small")
    except:
        invalid_number_element = None

print(FAKE, "\n")
WebDriverWait(driver, 5).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div/div/div/div/div[2]/div/div/div/div[3]/form/div/div/div/div/h5/label")))
counter += 1 # Another successful spam sent
driver.close()
